## Deconvolution of sarcoma methylomes


This repository contains code used in the analysis part of the publication 

**Deconvolution of sarcoma methylomes reveals varying degrees of immune cell iniltrates with association to genomic aberrations**

(https://doi.org/10.1186/s12967-021-02858-7)

Maintainer: [Malte Simon](mailto: malte.simon@dkfz-heidelberg.de)